1O. Windows 

2F. The vehicle disappears after it completes the east loop (about time 110). It disappears at the east loop_w_sb edge.

2H. Approximately 180

3G. 2.8 mpg

3I. 163, the headlights of the car are at the eastern tip of the edge.

4D. delivery: 57ffd9d7-f913-11ea-9df0-d283610663ec
    traffic: 76aaebb7-f913-11ea-9df0-d283610663ec

4I. delivery device TypeUuid: a62f387c-f910-11ea-9df0-d283610663ec

4N. Yes, communications slowed down when the other vehicle was added. 

4O. The sleep() function can be removed to help speed up the simulation

5C. iotery_response_delivery_vehicle = delivery_vehicle_cloud_connector.postData(
        deviceUuid=delivery_vehicle["uuid"], data=data)
        the response is structured: uuid, deviceUuid, commandTypeUuid, name, timestamp, isUnexecuted,setExecutedTimestamp, data

5E. Nothing

6A. There could be other internet of things controllers to use.

6B. More vehicles could be added to the simulation. 

6C. It seems that there are commands for the Traffic lights in Traci. Iotery can be used if the traffic lights were added as devices, and then commands. 

6D. 20 hours. No, it will probably take me a really long time.
